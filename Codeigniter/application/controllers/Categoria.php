<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Categoria extends CI_Controller {

   function __construct(){

    parent::__construct();


  }


	  public function index(){
        $result = $this->db->get('usuario');
        $data = array('consulta'=>$result);
		$this->load->view('home',$data);
    }
    /**
     * Obtiene todas las categorias y las carga
     */
    public function ver(){
        $result = $this->db->get('categoria');
        $data = array('consulta'=>$result);
		$this->load->view('VerCategoria',$data);
    }
    /**
     * Obtiene todos los productos y los carga
     */
    public function verP(){
      $result = $this->db->get('producto');
      $data = array('consulta'=>$result);
      $this->load->view('VerProductos',$data);
  }
  /**
   * Manda a llamar formulario para agregar categoria
   */
    public function agregar(){
    $this->load->view('AgregarCategoria');
  }
  /**
   * Manda a obtener la categoria y luego inserta en la base de datos
   */
  public function agregarP(){
    $infoCategorias = $this->ModelCategoria->obtenerCategorias();
    $this->load->view("AgregarProducto",array('infoCategorias' => $infoCategorias));
  }
  /**
   * Manda a que se elimine una categoria
   */
  public function eliminar(){
    $result = $this->db->get('categoria');
        $data = array('consulta'=>$result);
    $this->load->view('EliminarCategoria',$data);
  }
  /**Manda a eliminar un producto */
  public function eliminarP(){
    $result = $this->db->get('producto');
        $data = array('consulta'=>$result);
    $this->load->view('EliminarProducto',$data);
  }
  /**
   * Manda a modificar una categoria
   */
  public function modificar(){
    $result = $this->db->get('categoria');
        $data = array('consulta'=>$result);
    $this->load->view('EditarCategoria',$data);
  }
  /**
   * Manda a cargar los productos para modificar
   */
  public function modificarP(){
    $result = $this->db->get('producto');
        $data = array('consulta'=>$result);
    $this->load->view('ModificarProducto',$data);
  }
  /**
   * Elimina una categoria
   */
  public function eli(int $id){
      $this->ModelCategoria->eliminar($id);
      echo '<script type="text/javascript">alert("Categoría eliminada")</script>';
      $infouno=$this->ModelAdmin->uno();
      $infodos=$this->ModelAdmin->dos();
      $infotres=$this->ModelAdmin->tres();
      $this->load->view("Admin",array('infouno' => $infouno,'infodos' => $infodos,'infotres' => $infotres));
  }
  /**
   * Elimina un producto
   */
  public function eliP(int $id){

    $this->ModelCategoria->eliminarP($id);
    echo '<script type="text/javascript">alert("Producto eliminado")</script>';
    $infouno=$this->ModelAdmin->uno();
    $infodos=$this->ModelAdmin->dos();
    $infotres=$this->ModelAdmin->tres();
    $this->load->view("Admin",array('infouno' => $infouno,'infodos' => $infodos,'infotres' => $infotres));
}
/**
 * Manda a guardar una categoria
 */
  public function guardar(){
    
    if($this->input->post())
    {
      $nombre = $this->input->post("nombre");
      $this->ModelCategoria->guardar($nombre);
      echo '<script type="text/javascript">alert("Registro exitoso")</script>';
      $infouno=$this->ModelAdmin->uno();
      $infodos=$this->ModelAdmin->dos();
      $infotres=$this->ModelAdmin->tres();
      $this->load->view("Admin",array('infouno' => $infouno,'infodos' => $infodos,'infotres' => $infotres));

    }
  }
/**
 * Manda a guardar un producto
 */
  public function guardarP(){
    if($this->input->post())
    {
      $categoria = $this->input->post("categoria");
      $cateSeleccionada = $this->ModelCategoria->obtenerCategoriaSeleccionada($categoria);
      $idCategoria = $cateSeleccionada->id;
      $SKU = $this->input->post("SKU");
      $nombre = $this->input->post("nombre");
      $descripcion = $this->input->post("descripcion");
      $STOCK = $this->input->post("STOCK");
      $precio = $this->input->post("precio");
      $imagen = addslashes(file_get_contents($_FILES['imagen']['tmp_name']));  
      $this->ModelCategoria->guardarP($idCategoria,$SKU,$nombre,$descripcion,$STOCK,$precio,$imagen);
      echo '<script type="text/javascript">alert("Registro exitoso")</script>';
      $infouno=$this->ModelAdmin->uno();
      $infodos=$this->ModelAdmin->dos();
      $infotres=$this->ModelAdmin->tres();
      $this->load->view("Admin",array('infouno' => $infouno,'infodos' => $infodos,'infotres' => $infotres));

    }
  }
  /**
   * Obtiene todos los productos de una categoria
   */
  public function cargarEditar(int $id){
		$info = $this->ModelCategoria->getById($id);
		
		$this->load->view('ModificarCategoria',array('info' => $info));
   }
   /**
    * Carga todos los producctos de una categoria
    */
   public function cargarEditarP(int $id){
    $info = $this->ModelCategoria->getByIdP($id);
		$this->load->view('EditProducto',array('info' => $info));
    
  }
  /**
   * Manda a editar una categoria
   */
   public function editar()
	{
      if($this->input->post())
      {
          $id = $this->input->post("id");
          $nombre = $this->input->post("nombre");
          $this->ModelCategoria->actualizar($id, $nombre);
          echo '<script type="text/javascript">alert("Categoría modificada")</script>';
          $infouno=$this->ModelAdmin->uno();
          $infodos=$this->ModelAdmin->dos();
          $infotres=$this->ModelAdmin->tres();
          $this->load->view("Admin",array('infouno' => $infouno,'infodos' => $infodos,'infotres' => $infotres));
      }
  }
  /**
   * Manda a editar un producto
   */
  public function editarP()
	{
      if($this->input->post())
      {
          $id = $this->input->post("id");
          $SKU = $this->input->post("SKU");
          $nombre = $this->input->post("nombre");
          $descripcion = $this->input->post("descripcion");
          $STOCK = $this->input->post("STOCK");
          $precio = $this->input->post("precio");
          $this->ModelCategoria->actualizarP($id,$SKU,$nombre,$descripcion,$STOCK,$precio);
          echo '<script type="text/javascript">alert("Producto modificado")</script>';
          $infouno=$this->ModelAdmin->uno();
          $infodos=$this->ModelAdmin->dos();
          $infotres=$this->ModelAdmin->tres();
          $this->load->view("Admin",array('infouno' => $infouno,'infodos' => $infodos,'infotres' => $infotres));
      }
	}

}
