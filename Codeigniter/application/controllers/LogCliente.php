

<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class LogCliente extends CI_Controller {
	public function index()
	{
    }
    /**
     * Manda a cargar las categorias
     */
    public function verCategorias(){
        $result = $this->db->get('categoria');
        $data = array('consulta'=>$result);
		$this->load->view('CatalagoCategoria',$data);
    }
    /**
     * Manda a cargar el inico de la pagina del cliente
     */
    public function cliente(){
        $idCliente = $this->session->userdata('id');
        $infouno=$this->ModelCliente->uno($idCliente);
                    $infodos=$this->ModelCliente->dos($idCliente);
                    $total = $infouno * $infodos;
                    if($total == 0){
                        $infouno = 0;
                        $total = 0;
                    }
					 $this->load->view("Cliente",array('infouno' => $infouno,'infodos' => $total));
    }
    /**
     * Manda a cargar los productos de una categoria en especifico
     */
    public function catalogoProductos(int $idCatalogo){
        $info = $this->ModelCliente->cataProductos($idCatalogo);
		$this->load->view('CatalagoProductos',array('info' => $info));
    }
    /**
     * Manda a llamar el formulario para agregar al carrito la comprar deseada
     */
    public function carrito(int $idProducto){
        $this->load->view('CatalogoCarrito',array('idProducto' =>$idProducto));
    }
    /**
     * Manda a ver todos los productos que el usuario quiere comprar, pero no ha comprado
     */
    public function verCarrito(){
        $idCliente = $this->session->userdata('id');
        $info = $this->ModelCliente->cargarCarrito($idCliente);
		$this->load->view('VerCarrito',array('info' => $info));
    }
    /**
     * Agrega un producto al carrito
     */
    public function agregarCarrito(){
        if($this->input->post())
        {
          $idCliente = $this->session->userdata('id');
          $idProducto = $this->input->post("idProducto");
          $cantidadProducto = $this->input->post("cantidadProducto");
          $checkout = 0;

          $this->ModelCliente->agregarCarrito($idCliente,$idProducto,$cantidadProducto,$checkout);
          echo '<script type="text/javascript">alert("Agregado al carrito")</script>';
          $infouno=$this->ModelCliente->uno($idCliente);
                    $infodos=$this->ModelCliente->dos($idCliente);
                    $total = $infouno * $infodos;
                    if($total == 0){
                        $infouno = 0;
                        $total = 0;
                    }
					 $this->load->view("Cliente",array('infouno' => $infouno,'infodos' => $total));
    
        }
    }
    public function checkout($idCarrito){
       
    }
    /**
     * Manda a eliminar un producto del carrito
     */
    public function eliminar($idCarrito){
        $this->ModelCliente->eliminar($id);
        echo '<script type="text/javascript">alert("Producto eliminado del carrito")</script>';
        $infouno=$this->ModelCliente->uno($idCliente);
                    $infodos=$this->ModelCliente->dos($idCliente);
                    $total = $infouno * $infodos;
                    if($total == 0){
                        $infouno = 0;
                        $total = 0;
                    }
					 $this->load->view("Cliente",array('infouno' => $infouno,'infodos' => $total));
    
    }
}