

<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Registro extends CI_Controller {
	function __construct(){

		parent::__construct();
		$this->load->database();
	
	  }

	public function index()
	{
		$this->load->view("Registro");
    }
	
/**
 * Manda a registrar un usuario nuevo al sistema
 */
	public function registrar(){

		$nombre = $this->input->post("nombre");
		$apellidos = $this->input->post("apellido");
		$telefono = $this->input->post("telefono");
		$correo = $this->input->post("correo");
		$direccion = $this->input->post("direccion");
		$contrasena = $this->input->post("contrasena");
        $tipousu = $this->input->post("tipouso");
        $tipo = 0;
        if ($tipousu == "admin") {
            $tipo = 1;
        }else if ($tipousu == "cliente"){
            $tipo = 2;
        }   
	  $info = $this->RegistroUsu->guardar($nombre,$apellidos,$telefono,$correo,$direccion,$contrasena,$tipo);
	  
	  if($info){
        echo '<script type="text/javascript">alert("Registro exitoso")</script>';
        $this->load->view("login");
	  } else{
		echo '<script tpe="text/javascript">alert("Datos erroneos")</script>';
	  }
	}


}