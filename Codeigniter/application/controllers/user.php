

<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {
	public function index()
	{
		$this->load->view('login');
	}
	/**
	 * Manda a validar si el usuario existe en sistema, si exite verifica el tipo de 
	 * usuario y abre el form adecuado
	 */
	public function validar(){
	
		
		if($this->input->post()){
        $correo = $this->input->post("correo");
		$contrasena = $this->input->post("contrasena");
		
		$info =$this->loginVerificar->validar($correo,$contrasena);

		if($info){
			foreach($info as $row){
				if ($row->tipousu == 1){
					$this->session->set_userdata('id',$row->id);
					$infouno=$this->ModelAdmin->uno();
					$infodos=$this->ModelAdmin->dos();
					$infotres=$this->ModelAdmin->tres();
					$this->load->view("Admin",array('infouno' => $infouno,'infodos' => $infodos,'infotres' => $infotres));				
				}else if($row->tipousu == 2){
					$this->session->set_userdata('id',$row->id);
					$idCliente = $this->session->userdata('id');
					$infouno=$this->ModelCliente->uno($idCliente);
                    $infodos=$this->ModelCliente->dos($idCliente);
                    $total = $infouno * $infodos;
					 $this->load->view("Cliente",array('infouno' => $infouno,'infodos' => $total));
				}
			}			
		}else{
			echo '<script type="text/javascript">alert("Datos erroneos")</script>';
	   }
	}
	
}
/**
 * Manda a cargar la pagina incial del administrador
 */
public function administrador(){
	$infouno=$this->ModelAdmin->uno();
	$infodos=$this->ModelAdmin->dos();
	$infotres=$this->ModelAdmin->tres();
	$this->load->view("Admin",array('infouno' => $infouno,'infodos' => $infodos,'infotres' => $infotres));	
}
/**
 * Manda a cargar el formulario para registrarse
 */
public function registro(){
	$this->load->view('Registro');
}
/**
 * Cierra la sesion de los usuarios
 */
public function cerrar(){
	$l= array('id');
	$this->session->unset_userdata($l);

	$this->session->sess_destroy();

	$this->load->view("login");
}




}