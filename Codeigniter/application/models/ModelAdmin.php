<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ModelAdmin extends CI_Model{

    function __construct(){
        parent:: __construct();
        $this->load->database();
     }

    
     /**
      * Retorna la cantidad de usuario que existen en la base de datos
      */
    public function uno(){
        $query = $this->db->query('SELECT * FROM usuario');
        return $query->num_rows();
    }
    /**
     * Suma la cantidad de productos comprados
     */
    public function dos(){
        $query =$this->db->select_sum('cantprod');
        $query = $this->db->get('carrito')->row();        
        return $query->cantprod;
        
    }
    /**
     * Selecciona la cantidad de compras hechas en la tienda
     */
    public function tres(){
        $query =$this->db->select_sum('total');
        $query = $this->db->get('orden')->row();        
        return $query->total;
    }
}
