<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ModelCategoria extends CI_Model{



    public function __construct(){
        $this->load->database();
   }
   /**
    * Obtiene todos los usuarios del sistema
    */
   public function getPost(){
    $this->db->query("SELECT * FROM usuario");

    return $result->row();
}
/**
 * Guarda una categoria
 */
    public function guardar($nombre){

        $query=$this->db->insert("categoria",array("nombre"=> $nombre));  
    }
    /**
     * Elimina una categoria
     */
	public function eliminar(int $id)
	{
        $this->db->where('id', $id);
        $this->db->delete('categoria');
    }
    /**
     * Elimina un producto
     */
    public function eliminarP(int $id)
	{
        $this->db->where('id', $id);
        $this->db->delete('producto');
  }
  /**
   * Obtiene una categoria segun id
   */
    public function getById(int $id){
        $query = $this->db->get_where('categoria', array('id' => $id));
    
        if ($query->result()) {
            return $query->result();
          } else {
            return false;
          }
       
    }
    /**
     * Obtiene un producto segun id
     */
    public function getByIdP(int $id){
        $query = $this->db->get_where('producto', array('id' => $id));
    
        if ($query->result()) {
            return $query->result();
          } else {
            return false;
          }
       
    }
    /**
     * Modica una categoria en bases de datos
     */
    public function actualizar(int $id,string $nombre){
        $this->db->where('id', $id);
        $data  = array( 
            'nombre' => $nombre
        );    
        $this->db->update('categoria',$data);
    }
    /**
     * Modifica un producto en la base de datos
     */
    public function actualizarP(int $id,int $SKU,string $nombre,string $descripcion,
    int $STOCK,int $precio){
      $this->db->where('id', $id);
      $data  = array( 
        'SKU'=> $SKU,
        'nombre'=>$nombre,
        'descripcion'=>$descripcion,
        'STOCK'=>$STOCK,
        'precio'=>$precio
      );    
      $this->db->update('producto',$data);
  }
  /**
   * Obtiene todas las categorias
   */
    public function obtenerCategorias(){
        $query =  $this->db->query("SELECT * FROM categoria");
 
        if ($query->result()) {
         return $query->result();
       } else {
         return false;
       }
     }
     /**
      * Obtiene la categoria segun su nombre
      */
     public function obtenerCategoriaSeleccionada(string $nombreCategoria){
      return $this->db->where('nombre', $nombreCategoria)->get('categoria')->row();
    }

  
    /**
     * Guarda un producto en la base de datos
     */
 
    public function guardarP(int $idC,int $SKU,string $nombre,string $descripcion,
    int $STOCK,int $precio,string $imagen){

        $query=$this->db->insert("producto",array("SKU"=> $SKU,"nombre"=> $nombre,"descripcion"=> $descripcion,
        "imagen"=> $imagen,"idcategoria"=> $idC,"STOCK"=> $STOCK,"precio"=> $precio));  
       
    }

}
