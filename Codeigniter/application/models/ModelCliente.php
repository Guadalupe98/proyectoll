<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ModelCliente extends CI_Model{



    public function __construct(){
        $this->load->database();
   }
   /**
    * Obtiene  productos de una categoria
    */
   public function cataProductos(int $idCatalogo){
      $query = $this->db->get_where('producto', array('idcategoria' => $idCatalogo));

      if ($query->result()) {
          return $query->result();
        } else {
          return false;
        }
    }
/**
 * Agregar productos al carrito
 */
    public function agregarCarrito(int $idCliente,int $idProducto,int $cantidadProducto,int $checkout){
      $query=$this->db->insert("carrito",array("checkout"=> $checkout,"cantprod"=> $cantidadProducto,"idProducto"=> $idProducto,
        "idUsuario"=> $idCliente));
    }

    /**
     * Obtiene la cantidad de productos comprados por un usuario
     */
  public function uno($idUsuario){
      $query =$this->db->select_sum('cantprod');
      $query = $this->db->get_where('carrito', array('idUsuario' => $idUsuario,'checkout' => 1))->row();        
      return $query->cantprod;
      
  }
  /**
   * Obtiene el precio total de los productos comprados por un usuario
   */
  public function dos($idUsuario){
    $query = $this->db->get_where('carrito', array('idUsuario' => $idUsuario))->row();
    $idProducto = $query->idProducto;

    $query =$this->db->select_sum('precio');
    $query = $this->db->get_where('producto', array('id' => $idProducto))->row();        
    return $query->precio;
}
/**
 * Elimina un producto del carrito
 */
public function eliminar($idCarrito){
  $this->db->where('id', $idCarrito);
  $this->db->delete('carrito');
  
}
/**
 * Carga todos los productos no confirmados de compra de un usuario en especifico
 */
public function cargarCarrito($idCarrito){
  $query = $this->db->get_where('carrito', array('idUsuario' => $idCarrito,'checkout' => 0));
  
  return $query->result();
}

}
