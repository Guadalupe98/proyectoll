<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class RegistroUsu extends CI_Model{

    function __construct(){
        parent:: __construct();
        $this->load->database();
     }

    
/**
 * Guarda un nuevo usuario al sistema, es decir en la base de datos
 */
    public function guardar(string $nombre,string $apellido,string $telefono,string $correo,
    string $direccion,string $contrasena, int $tipoUso){

        
         $query = $this->db->insert("usuario",array("nombre"=> $nombre,"apellido"=> $apellido,
         "tel"=>$telefono,"correo"=>$correo,"direccion"=>$direccion,"contrasena"=>$contrasena,
         "tipousu"=>$tipoUso));

            if ($this->db->affected_rows() > 0) {
                return true;
             } else {
                return false;
            }

        }

    }
