<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class loginVerificar extends CI_Model{

    function __construct(){
        parent:: __construct();
        $this->load->database();
     }
     /**
      * Valida si el correo y contrasena recibidos existen dentro de la base de datos
      */
     public function validar(string $correo, string $contrasena){
        $query = $this->db->get_where('usuario', array('correo' => $correo, 'contrasena' => $contrasena));
    
        if ($query->result()) {
            return $query->result();
          } else {
            return false;
          }
     }

}
