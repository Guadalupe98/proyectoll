
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Administrador</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>../CSS/styleInicioAdmin.css">

</head>
<body>
    <div class="container">
        <div class="row">
            <div class="col-sm">
            <header>
    <nav class="navegacion">
      <ul class="menu">
      <li><a href="/index.php/LogCliente/cliente">Pagina Inicial</a>
        </li>
        <li><a href="CatalogoC.php">Catálago de producto</a>
        </li>
                <li><a href="comprasRealizadas.php">Compras realizadas</a>
                </li>
                <li><a href="verCarrito.php">Ver carrito</a>
                </li>
                <li>
            <a href="/index.php/User/cerrar" name="salir">Cerrar Sesión</a> 
            </li>
                
            </ul>

    </nav>
    </header>          

    <form action="/index.php/LogCliente/agregarCarrito" method="POST" > 
                <div class="form-group">
                <div style = "text-align: center">
                    <h3>Información Personal</h3><br> <br>
                    <h7>Del tipo de producto</h7><br> <br>
                    <label for = "pro"></label>
                    <input id="p" type="text" name= "idProducto" value="<?php echo $idProducto?>"><br> <br>                
                    <label for = "Cantidad"></label>
                    <input id="cant" type="text" name= "cantidadProducto"placeholder="Digite la cantidad" required><br>
                    <button name="guardar" class="Registro btn btn-primary">Agregar</button>
                    </div>                                               
                </div>                               
            </form>
            </div>
        </div>
    </div>
</body>
</html>


