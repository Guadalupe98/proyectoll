<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>VerCtegoria</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>../CSS/styleInicioAdmin.css">
</head>
<body>
<div class="container">
        <div class="row">
            <div class="col-sm">
            <header>
    <nav class="navegacion">
      <ul class="menu">
      <li><a href="/index.php/User/administrador">Pagina Inicial</a>
        </li>
        <li><a href="#">Categorías</a>
          <ul class="submenu">
            <li><a href="/index.php/Categoria/ver">Ver Categorías</a></li>
            <li><a href="/index.php/Categoria/agregar">Agregar Categoría</a></li>
            <li><a href="/index.php/Categoria/modificar">Editar Categoría</a></li>
            <li><a href="/index.php/Categoria/eliminar">Eliminar Categoría</a></li>
          </ul>
        </li>
                <li><a href="#">Productos</a>
                <ul class="submenu">
            <li><a href="/index.php/Categoria/verP">Ver Productos</a></li>
            <li><a href="/index.php/Categoria/agregarP">Agregar Producto</a></li>
            <li><a href="/index.php/Categoria/modificarP">Editar Producto</a></li>
            <li><a href="/index.php/Categoria/eliminarP">Eliminar Producto</a></li>
          </ul>
          </li>
          <li><a href="/index.php/User/cerrar" name="salir">Cerrar Sesión</a></li>  
    </nav>

    </header>   
    <br><br><div style="text-align:center;">       
    <table id="tablaCat" border="1"  width="1110" height="100" >
            <tr>
                <td bgcolor="rosybrown">NOMBRE</td>
            </tr>
        <?php

        foreach($consulta->result() as $fila){ ?>
            <tr>
                <td bgcolor="rosybrown"><?php echo $fila->nombre;?></td>
            </tr>
          <?php }?>
    </table> 
    </div>
    </div>

            </div>
        </div>
    </div>
</body>
</html>